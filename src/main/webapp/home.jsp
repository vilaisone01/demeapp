<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <!-- Bootstrap JS -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-dY6w6j8qG3ziMgjXOw4YJ8av1iXZtxqZ7oThf4ZgqwADa8d7KCYO2bhmFqLURuZC" crossorigin="anonymous"></script>
  <meta charset="UTF-8">
  <title>User Data</title>
  <style>
    table {
      border-collapse: collapse;
    }
    th, td {
      padding: 8px;
      text-align: center;
    }
    .avatar-img {
      display: block;
      margin: 0 auto;
      width: 60px;
      height: 60px;
    }
  </style>
</head>
<body>
  <jsp:include page="navbar.jsp" />
  <h2 style="text-align: center;">Show all users</h2>
  <div class="text-right">
    <button type="button" class="btn btn-primary" onclick="redirectToRegister()">Add new user</button>
  </div>

  <table id="userTable" class="table">
    <thead>
      <tr>
        <th>No</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Username</th>
        <th>Avatar</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <!-- Table body will be populated dynamically -->
    </tbody>
  </table>

  <script>
  function redirectToRegister(userId) {
	  window.location.href = `<%= request.getContextPath() %>/register.jsp?id=${userId}`;
	}


    // Fetch data from API and populate the table
    fetch('https://www.melivecode.com/api/users')
      .then(response => response.json())
      .then(data => {
        const users = data;

        // Populate the table with the fetched data
        const tableBody = document.querySelector('#userTable tbody');

        users.forEach(user => {
          const row = document.createElement('tr');

          const noCell = document.createElement('td');
          noCell.textContent = user.id;
          row.appendChild(noCell);

          const fnameCell = document.createElement('td');
          fnameCell.textContent = user.fname;
          row.appendChild(fnameCell);

          const lnameCell = document.createElement('td');
          lnameCell.textContent = user.lname;
          row.appendChild(lnameCell);

          const usernameCell = document.createElement('td');
          usernameCell.textContent = user.username;
          row.appendChild(usernameCell);

          const avatarCell = document.createElement('td');
          const avatarImg = document.createElement('img');
          avatarImg.src = user.avatar; // Assuming the API provides the image URL in the 'avatar' property
          avatarImg.classList.add('avatar-img'); // Add the CSS class to the image element
          avatarCell.appendChild(avatarImg);
          row.appendChild(avatarCell);

          const actionCell = document.createElement('td');

          const editButton = document.createElement('button');
          editButton.textContent = 'Edit';
          editButton.classList.add('btn', 'btn-primary', 'mr-2');
          editButton.addEventListener('click', () => {
            redirectToRegister(user.id);
          });
          actionCell.appendChild(editButton);

          const deleteButton = document.createElement('button');
          deleteButton.textContent = 'Delete';
          deleteButton.classList.add('btn', 'btn-danger');
          deleteButton.addEventListener('click', () => {
            // Handle delete functionality here
            // You can show a confirmation dialog and send a DELETE request to the API
          });
          actionCell.appendChild(deleteButton);

          row.appendChild(actionCell);

          tableBody.appendChild(row);
        });
      })
      .catch(error => {
        console.log('An error occurred:', error);
      });
  </script>
</body>
</html>
