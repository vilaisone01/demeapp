<%
Cookie[] cookies = request.getCookies();
boolean foundCookie = false;
String cookies_userId = "";
String cookies_password = "";
String cookies_remember = "";

for (int i = 0; i < cookies.length; i++) {
    Cookie c = cookies[i];
    if (c.getName().equals("userid")) {
        cookies_userId = c.getValue();
        foundCookie = true;
    }
    if (c.getName().equals("userpassword")) {
        cookies_password = c.getValue();
        foundCookie = true;
    }
}
%>

<%@page import="java.io.File"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
          crossorigin="anonymous">
    <title>Login</title>
    
</head>
<body>
<div align="center">
    <h1>Login form</h1>
</div>

<form >
    <div class="px-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="exampleInputEmail1"
                   aria-describedby="emailHelp" placeholder="name@example.com"
                   value="<%=cookies_userId%>">
            <small id="emailHelp" class="form-text text-muted">We'll
                never share your email with anyone else.</small>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1"
                   value="<%=cookies_password%>">
        </div>
        <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="exampleCheck1"
                   <% if (foundCookie) { %> checked <% } %>>
            <label class="form-check-label" for="exampleCheck1">
                Remember me</label>
        </div>
        <div align="center">
            <button type="submit" class="btn btn-primary" onclick="postLogin()" >Submit</button>
            <button type="button" class="btn btn-link"
                    onclick="redirectToRegister()">Register</button>
        </div>
    </div>
</form>
</body>
<script>
    function redirectToRegister() {
        window.location.href = '<%=request.getContextPath()%>/register.jsp';
    }

    function postLogin() {
        const email = document.getElementById('exampleInputEmail1').value;
        const password = document.getElementById('exampleInputPassword1').value;
        const rememberUser = document.getElementById('exampleCheck1').checked;

        // Save the user and password as cookies if the "Remember user" checkbox is checked
        if (rememberUser) {
            document.cookie = "userid=" + email + ";path=/";
            document.cookie = "userpassword=" + password + ";path=/";
        } else {
            // If the checkbox is not checked, clear the existing cookies
            document.cookie = "userid=;expires=Thu, 01 Jan 1970 00:00:00 UTC;path=/;";
            document.cookie = "userpassword=;expires=Thu, 01 Jan 1970 00:00:00 UTC;path=/;";
        }

        // Perform the login request or any other desired action
        const data = {
            email: email,
            password: password
        };
        axios.post('https://www.melivecode.com/api/login', data)
            .then(response => {
                console.log(response.data);
                // Handle the response
                window.location.href = '<%=request.getContextPath()%>/home.jsp';
            })
            .catch(error => {
                console.error(error);
  

                // Handle the error
            });
    }
</script>
</html>
