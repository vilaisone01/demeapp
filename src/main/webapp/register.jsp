<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<style>
.dropzone {
	width: 300px;
	height: 200px;
	border: 2px dashed #ccc;
	border-radius: 4px;
	padding: 20px;
	text-align: center;
	font-size: 18px;
}

.dropzone i {
	margin-bottom: 10px;
}
</style>

<head>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
<meta charset="ISO-8859-1">
<title>Register page</title>
</head>


<body>

	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10">
				<div class="card">
					<div class="card-header" align="center">
						<div></div>
						<h2>Register form</h2>
						<h2>ຟອມລົງທະບຽນ | 등록 양식</h2>
					</div>
					<div class="card-body">
						<form action="" method="post">
							<!-- <form action="loginServlet" method="post"> -->
							<div class="px-3; py-4">
								<div class="form-group">
									<label>First name</label> <input type="text"
										class="form-control" id="InputFname">
								</div>

								<div class="form-group">
									<label> Last name</label> <input type="text"
										class="form-control" id="InputLname">
								</div>

								<div class="form-group">
									<label> User name</label> <input type="text"
										class="form-control" id="InputUsername">
								</div>

								<div class="form-group">
									<label> Email</label> <input type="email" class="form-control"
										id="InputEmail">
								</div>

								<div class="form-group">
									<label> password</label> <input type="password"
										class="form-control" id="InputPassword">
								</div>

								<div class="form-group">
									<label for="InputImage">Upload Image</label> <input type="file"
										class="form-control-file" id="InputImage" name="image"
										accept="image/*">
								</div>



								<div align="center">
									<button type="submit" class="btn btn-primary"
										onclick="postData()">
										<i class="fas fa-check"></i> Submit
									</button>
								</div>

							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>


</body>

<script>
	function postData() {
		const data = {
			fname : document.getElementById('InputFname').value,
			lname : document.getElementById('InputLname').value,
			username : document.getElementById('InputUsername').value,
			email : document.getElementById('InputEmail').value,
			password : document.getElementById('InputPassword').value,
			avatar : document.getElementById('InputImage').files[0]
		};
		axios.post('https://www.melivecode.com/api/users/create', data)
	      .then(response => {
	        console.log(response.data);
	        // Handle the response
	        window.location.href = '<%= request.getContextPath() %>/home.jsp';
	      })
	      .catch(error => {
	        console.error(error);
	        // Handle the error
	      });
	}
</script>


</html>